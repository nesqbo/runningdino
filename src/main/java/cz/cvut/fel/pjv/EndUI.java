package cz.cvut.fel.pjv;

import cz.cvut.fel.pjv.MainPanel;
import cz.cvut.fel.pjv.Models.PlayerService;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class EndUI extends JPanel{
    private static final Logger LOGGER = Logger.getLogger(EndUI.class.getName());
    private boolean log;
    private MainPanel panel;
    private ImageIcon gifIcon;
    private BufferedImage[] images = new BufferedImage[2];
    public EndUI(MainPanel panel) {
        this.panel = panel;
        getCryCatImage();
        getDanceCatImage();
        this.log = panel.isEnableLogging();
    }

    public void draw(Graphics g){
        g.setFont(new Font("Arial", 16, 48));
        g.setColor(Color.RED);
        if (panel.isGameEnded() == true && panel.isPlayerWon()){
            g.drawImage(images[0], 360,360, null);
            g.drawString("You've won!", 100, 360);
        } else if (panel.isGameEnded() && !panel.isPlayerWon()){
            g.drawImage(images[1], 360,360, null);
            g.drawString("You've died, try again", 100, 360);
            g.drawString("AHAHHAHAHAHAHHAHAHAHAHAHHAHAHAHAHHAAHHAAHHAHAHHAHAH", 0, 420);
        }
    }

    public void getDanceCatImage(){
        try {
            images[0] = ImageIO.read(getClass().getResourceAsStream("/gifs/f2d55ae2cd07ae2a4b5aa61cde061d4c.gif"));
        } catch (IOException | NumberFormatException e) {
            if (log){
                LOGGER.log(Level.SEVERE, "Can't load image");
            }
            e.printStackTrace();
        }
    }

    public void getCryCatImage(){
        try {
            images[1] = ImageIO.read(getClass().getResourceAsStream("/gifs/banana-cat-crying.gif"));
        } catch (IOException | NumberFormatException e) {
            if (log){
                LOGGER.log(Level.SEVERE, "Can't load image");
            }
            e.printStackTrace();
        }
    }

    public boolean isLog() {
        return log;
    }

    public void setLog(boolean log) {
        this.log = log;
    }

    public MainPanel getPanel() {
        return panel;
    }

    public void setPanel(MainPanel panel) {
        this.panel = panel;
    }

    public ImageIcon getGifIcon() {
        return gifIcon;
    }

    public void setGifIcon(ImageIcon gifIcon) {
        this.gifIcon = gifIcon;
    }

    public BufferedImage[] getImages() {
        return images;
    }

    public void setImages(BufferedImage[] images) {
        this.images = images;
    }
}
