package cz.cvut.fel.pjv;


import cz.cvut.fel.pjv.Entities.Food;
import cz.cvut.fel.pjv.Entities.Key;
import cz.cvut.fel.pjv.Entities.Monster;
import cz.cvut.fel.pjv.Entities.Player;
import cz.cvut.fel.pjv.Models.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import java.util.List;
import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainPanel extends JPanel implements Runnable {
    private static final Logger LOGGER = Logger.getLogger(MainPanel.class.getName());
    private boolean enableLogging = false; //true to log, false to not log
    Random rand = new Random();
    public static int SIZE = 24;
    private int x = 0;
    private int y = 0;
    private List<BufferedImage> backgroundImages;
    private boolean gameEnded = false;//gameEnded == false znamena, ze hra jede, jinak
    private boolean playerWon; //if gameEnded and playerWon then player Won, if gameEnded and !playerWon then player lost
    private int FPS = 60;
    private double drawInterval = 1000000000 / FPS;
    private Thread gameThread;

    private EndUI endUI = new EndUI(this);
    private Monster[] monster = {new Monster(enableLogging), new Monster(enableLogging), new Monster(enableLogging), null, null, null, null, null, null, null, null, null, null};
    private Key[] key = {new Key(enableLogging), new Key(enableLogging), new Key(enableLogging), null, null, null, null};
    private Listener listener = new Listener();
    private Player player = new Player(listener);
    private Food[] food = {new Food(enableLogging), new Food(enableLogging), new Food(enableLogging), new Food(enableLogging)};
    private Food[] newFood;
    private Stats stats = new Stats(player.getHearts(), player.getKilledMonstersCount());
    private LoadSave loadSave = new LoadSave(player, player.getMaxAmountOfMonsters() - player.getKilledMonstersCount(), player.getKeyCount(), this);
    private MapModel map = new MapModel(player, loadSave, enableLogging);
    private ObjectService objectService = new ObjectService(player, key, food, map, enableLogging);
    private PlayerService playerService = new PlayerService(player, listener, loadSave, map, this);
    private MonsterService monsterService = new MonsterService(player, monster, loadSave, map, this);

    /**
     * Constructs a new MainPanel object which "owns" all of the objects that are being used for and in
     * the game.
     * Initializes the top panel with load/save statistics and load map controls.
     * Loads background images and sets up the key listener.
     * Initializes monsters and keys.
     */
    public MainPanel()  {

        int keycount = 0;
        for (int i = 0; i < key.length; i++){
            if (key[i] != null){
                keycount++;
            }
        }
        int aliveMonsterCount = 0;
        for (int i = 0; i < monster.length; i++) {
            if (monster[i] != null){
                aliveMonsterCount++;
            }
        }
        JPanel topPanel = new JPanel(new BorderLayout());
        topPanel.add(stats);
        topPanel.add(loadSave, BorderLayout.NORTH);
        add(topPanel);

        // load background images
        backgroundImages = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            String filename = "/floors/" + i + ".png";
            try {
                BufferedImage img = ImageIO.read(getClass().getResourceAsStream(filename));
                backgroundImages.add(img);
            } catch (IOException e) {
                if (isEnableLogging() == true) {
                    LOGGER.log(Level.SEVERE, "the tile image has failed to load");
                }
                e.printStackTrace();
            }
        }

        this.addKeyListener(listener);
        this.setFocusable(true);
        setMonsters();
        setKeys();
    }

    /**
     * Sets the positions of the monsters on the map.
     * The monsters will be spawned at predetermined locations on each map.
     * The newX and newY coordinates are calculated based on the current monster's position in the array.
     * The updated positions are then set for each monster respectfully in the array.
     */
    public void setMonsters(){
        int count = 0;
        for (int i=0; i<monster.length;i++){
            if (monster[i] != null){
                count++;
            }
        }
        if (count == 0){
            for (int i = 0; i < monster.length-2; i++) {
                monster[i] = new Monster(isGameEnded());
            }
        }
        int newX;
        int newY;
        for (int i =0; i<monster.length; i++){
            //vzdy se na kazde mape budou spawnovat na tomto miste
            if (monster[i] != null){
                if (i%2 != 0){
                    newX = monster[i].getX() + i*SIZE;
                    newY = monster[i].getY() + i;
                } else {
                    newX = (monster[i].getX() + i*SIZE)/4;
                    newY = (monster[i].getY()  +i*SIZE)/2;
                }
                monster[i].setX(newX);
                monster[i].setY(newY);
            }
        }
    }

    /**
     * Sets the positions of the keys on the map.
     * The keys will be spawned at predetermined locations on each map.
     * The newX and newY coordinates are calculated based on the current key's position in the array.
     * The updated positions are then set for each key in the array.
     */
    public void setKeys(){
        int count = 0;
        for (int i=0; i<key.length;i++){
            if (key[i] != null){
                count++;
            }
        }
        if (count == 0){
            for (int i = 0; i < key.length-2; i++) {
                key[i] = new Key(isGameEnded());
            }
        }

        int check=0;
        for (int i = 0; i < key.length; i++) {
            if (key[i] !=null){
                check++;
            }
        }
        if (check > 3){
            for (int j=3; j<key.length; j++){
                key[j] = null;
            }
        }

        int newX;
        int newY;
        for (int i =0; i<key.length; i++){
            //vzdy se na kazde mape budou spawnovat na tomto miste
            if (key[i] != null){
                if (i>=2){
                    newX = (2 + (320 + 70 ))/i;
                    newY = (72 + (i + 98))/i;

                } else {
                    newX = 2 + (320 + 70 * i);
                    newY = 72 *i+ (i*SIZE + 98);

                }
                key[i].setY(newY);
                key[i].setX(newX);
            }
        }
    }

    /**
     * Overrides the paintComponent method to paint the game components on the panel.
     * @param g The Graphics object used for painting.
     */
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (!isGameEnded()){

            map.draw(g);
            for (int i = 0; i < key.length; i++) {
                if (key[i] != null) {
                    key[i].draw(g);
                }
            }

            for (int j = 0; j < monster.length; j++) {
                if (monster[j] != null) {
                    monster[j].draw(g);
                }
            }
            for (int j = 0; j < food.length; j++) {
                if (food[j] != null) {
                    food[j].draw(g, 98 + (48 * j + 48 * j * 2) - 2, 194 + (98 * j + 24 * j * 2) / 2);
                }
            }
            player.draw(g);
        } else {
            endUI.draw(g);
        }
        stats.draw(g, player.getHearts(), player.getKilledMonstersCount(), player.getKeyCount());

    }

    /**
     * Updates the game state by calling the update methods of all components that are needed for the game.
     * This method is called in the game loop to keep the game state updated at all times while the game is
     * running.
     */
    public void update(){
        map.update();
        updateObjects();
        playerService.update();
        monsterService.update();
        objectService.update();

        for (int j = 0; j<monster.length; j++){
            if (monster[j]!= null){
                    monster[j].update();
            }
        }
        loadSave.update();
    }

    /**
     * Updates the game objects and related components.
     * This method is responsible for updating the key count, monster count, and after they have been updated,
     * it returns the game to a non changing state, otherwise the game's amount of monster and keys would
     * constantly be updated wrongly.
     */
    void updateObjects(){
        updateKeyCount();
        updateMonsterCount();
        loadSave.setUpdateMap(false);
    }

    /**
     * Updates the key count and the corresponding key objects that will be drawn out after this update.
     * This method is responsible for updating the key count based on the player's maximum keys and
     * updating the key objects accordingly so there wouldn't be an infinite glitch in the game's amount of
     * keys.
     * If the map needs to be updated (specified by the update state of load/save component), the key
     * objects are adjusted to match the updated key count.
     */
    void updateKeyCount() {
        if (loadSave.isUpdateMap()) {
            int toDraw = player.getMaxKeys() - player.getKeyCount();
            int currentLength = key.length;
            if (toDraw < currentLength) {
                for (int i = toDraw; i < currentLength; i++) {
                    key[i] = null;
                }
            }
            for (int j = 0; j < toDraw; j++) {
                key[j] = new Key(isEnableLogging());
            }
            setKeys();
        }
    }

    /**
     * Updates the monster count and the corresponding monster array to be of correct length so a correct
     * amount will be drawn out even after loading statistics from a file.
     * This method is responsible for updating the monster count based on the player's maximum amount of
     * monsters and the killed monsters count,
     * and updating the monster objects accordingly.
     * If the map needs to be updated (specified by the update state of load/save component), the monster
     * objects are adjusted to match the updated monster count.
     */
    void updateMonsterCount() {
        if (loadSave.isUpdateMap()) {
            int toDraw = player.getMaxAmountOfMonsters() - player.getKilledMonstersCount();
            int currentLength = monster.length;
            if (toDraw < currentLength && toDraw >= 0) {
                for (int i = toDraw; i < currentLength; i++) {
                    monster[i] = null;
                }
            }
            for (int j = 0; j < toDraw; j++) {
                monster[j] = new Monster(isEnableLogging());


            }
            setMonsters();
        }
    }

    /**
     * The main game loop that runs continuously when the game is running.
     * This method is responsible for updating and repainting the game at a specific interval.
     * It uses a delta value to ensure a consistent update rate, based on the specified draw interval.
     * The game loop continues indefinitely until explicitly terminated by closing the game frame.
     */
    @Override
    public void run() {

        double delta = 0;
        long lastTime = System.nanoTime();
        long currentTime;

        while (true) {
            currentTime = System.nanoTime();
            delta += (currentTime - lastTime) / drawInterval;
            lastTime = currentTime;


                if (delta >= 1) {
                    delta--;
                if (!isGameEnded()) {

                update();
                repaint();
                }
            }

        }
    }

    /**
     * Starts the game by creating and starting a new thread.
     * The game thread is responsible for executing the game loop and updating the game state.
     */
    public void startGame(){
        gameThread = new Thread(this);
        gameThread.start();
    }
    @Override
    public int getX() {
        return x;
    }
    public void setX(int x) {
        this.x = x;
    }
    @Override
    public int getY() {
        return y;
    }
    public void setY(int y) {
        this.y = y;
    }
    public boolean isGameEnded() {
        return gameEnded;
    }

    /**
     * Sets the game's end state so the ending strings can be drawn out only when the game has ended.
     * @param gameEnded the boolean value indicating whether the game has ended
     */
    public void setGameEnded(boolean gameEnded) {
        this.gameEnded = gameEnded;
    }

    public Monster[] getMonster() {
        return monster;
    }

    public Random getRand() {
        return rand;
    }

    public void setRand(Random rand) {
        this.rand = rand;
    }

    public static int getSIZE() {
        return SIZE;
    }

    public static void setSIZE(int SIZE) {
        MainPanel.SIZE = SIZE;
    }

    public List<BufferedImage> getBackgroundImages() {
        return backgroundImages;
    }

    public void setBackgroundImages(List<BufferedImage> backgroundImages) {
        this.backgroundImages = backgroundImages;
    }

    public int getFPS() {
        return FPS;
    }

    public void setFPS(int FPS) {
        this.FPS = FPS;
    }

    public double getDrawInterval() {
        return drawInterval;
    }

    public void setDrawInterval(double drawInterval) {
        this.drawInterval = drawInterval;
    }

    public Thread getGameThread() {
        return gameThread;
    }

    public void setGameThread(Thread gameThread) {
        this.gameThread = gameThread;
    }

    public void setMonster(Monster[] monster) {
        this.monster = monster;
    }

    public Key[] getKey() {
        return key;
    }

    public void setKey(Key[] key) {
        this.key = key;
    }

    public Listener getListener() {
        return listener;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Food[] getFood() {
        return food;
    }

    public void setFood(Food[] food) {
        this.food = food;
    }

    public Food[] getNewFood() {
        return newFood;
    }

    public void setNewFood(Food[] newFood) {
        this.newFood = newFood;
    }

    public Stats getStats() {
        return stats;
    }

    public void setStats(Stats stats) {
        this.stats = stats;
    }

    public LoadSave getLoadSave() {
        return loadSave;
    }

    public void setLoadSave(LoadSave loadSave) {
        this.loadSave = loadSave;
    }

    public MapModel getMap() {
        return map;
    }

    public void setMap(MapModel map) {
        this.map = map;
    }

    public ObjectService getObjectService() {
        return objectService;
    }

    public void setObjectService(ObjectService objectService) {
        this.objectService = objectService;
    }

    public PlayerService getPlayerService() {
        return playerService;
    }

    public void setPlayerService(PlayerService playerService) {
        this.playerService = playerService;
    }

    public MonsterService getMonsterService() {
        return monsterService;
    }

    public void setMonsterService(MonsterService monsterService) {
        this.monsterService = monsterService;
    }

    public boolean isEnableLogging() {
        return enableLogging;
    }

    public void setEnableLogging(boolean enableLogging) {
        this.enableLogging = enableLogging;
    }

    public boolean isPlayerWon() {
        return playerWon;
    }

    public void setPlayerWon(boolean playerWon) {
        this.playerWon = playerWon;
    }
}