package cz.cvut.fel.pjv;

import cz.cvut.fel.pjv.Models.LoadSave;

import javax.swing.*;
import java.awt.*;
import java.text.DecimalFormat;
import java.util.logging.Logger;

public class Stats extends JPanel {


    double playTime;
    DecimalFormat dFormat = new DecimalFormat("#0.00");

    private int hearts, monstersKilled;
    /**
     * Constructs a new Stats object with the specified number of hearts and monsters killed.
     * @param hearts          the number of hearts
     * @param monstersKilled  the number of monsters killed
     */
    public Stats(int hearts, int monstersKilled){
        this.hearts = hearts;
        this.monstersKilled = monstersKilled;
    }

    /**
     * Draws the game statistics on the specified Graphics object.
     * @param g              the Graphics object to draw on
     * @param hearts         the number of hearts of the player
     * @param monstersKilled the number of monsters killed by the player
     * @param keys           the number of keys that have been picked up by the player
     */
    public void draw(Graphics g, int hearts, int monstersKilled, int keys){
        g.setFont(new Font("Arial", 16, 16));
        g.drawString("Hearts: " + hearts, 60, 60);
        g.drawString("Monsters killed: " + monstersKilled, 400, 60);
        g.drawString("Keys: " + keys, 550, 60);
//        g.drawString("To kill: " + );
        playTime += (double)1/60;//gets called 60 times a second
        g.drawString("Time:" + dFormat.format(playTime), 24*11, 65);

    }

    public double getPlayTime() {
        return playTime;
    }

    public void setPlayTime(double playTime) {
        this.playTime = playTime;
    }

    public DecimalFormat getdFormat() {
        return dFormat;
    }

    public void setdFormat(DecimalFormat dFormat) {
        this.dFormat = dFormat;
    }

    public int getHearts() {
        return hearts;
    }

    public void setHearts(int hearts) {
        this.hearts = hearts;
    }

    public int getMonstersKilled() {
        return monstersKilled;
    }

    public void setMonstersKilled(int monstersKilled) {
        this.monstersKilled = monstersKilled;
    }
}
