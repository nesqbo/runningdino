package cz.cvut.fel.pjv.Models;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.*;

import cz.cvut.fel.pjv.Entities.Player;
import cz.cvut.fel.pjv.MainPanel;
import org.json.JSONObject;
import org.json.JSONTokener;


public class LoadSave extends JPanel {
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = Logger.getLogger(LoadSave.class.getName());
    private boolean log;
    private JButton loadButton;
    private JButton saveButton;
    private JButton levelButton;
    private int[][] levels;
    private JSONObject json;
    private boolean updateMap = false;
    private MainPanel panel;
    private Player player;

    private String fileName = "src/main/resources/data/gameData.json";
    private String JSON_FILE_PATH = "src/main/resources/data/gameData.json";
    public LoadSave(Player player, int aliveMonstersCount, int keyCount, MainPanel panel) {
        this.panel = panel;
        this.log = panel.isEnableLogging();
        this.player = player;
        loadButton = new JButton("Load from file");
        saveButton = new JButton("Save");
        levelButton = new JButton("Level");

        loadButton.setFocusable(false);
        saveButton.setFocusable(false);
        levelButton.setFocusable(false);

        /**

         Adds an ActionListener to the loadButton component.
         When the loadButton is clicked, the actionPerformed method is invoked.
         This method sets the updateMap flag to true, loads game data from a JSON file,
         and calls the gameStarted method to start or resume the game.
         @param player The Player object to be updated with the loaded game data.
         @param JSON_FILE_PATH The file path of the JSON file containing the game data.
         */
        loadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateMap = true;
                loadFromFile(player, JSON_FILE_PATH);
                gameStarted();
            }
        });

        /**

         Adds an ActionListener to the saveButton component.
         When the saveButton is clicked, the actionPerformed method is invoked.
         This method calls the saveToFile method to save the game data of the player.
         @param player The Player object containing the game data to be saved.
         */
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveToFile(player);
            }
        });

        /**

         Adds an ActionListener to the levelButton component.
         When the levelButton is clicked, the actionPerformed method is invoked.
         This method calls the loadLevelFromFile method to load level data from a file,
         and then calls the gameStarted method to update the game state.
         */
        levelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (panel.isGameEnded()){
                    panel.setGameEnded(false);
                    player.setKeyCount(0);
                    player.setHearts(100);
                    player.setKilledMonstersCount(0);

                    panel.setMonsters();
                    panel.setKeys();
                    panel.setPlayerWon(false);
//                    System.out.println("setting new stats, hearts: " + player.getHearts() + " keys" + player.getKeyCount());
                }
                loadLevelFromFile();
                gameStarted();
            }
        });


        add(loadButton);
        add(saveButton);
        add(levelButton);

        setPreferredSize(new Dimension(400, 30));

        readDefaultMap();

    }

    public void readDefaultMap() {
        try (BufferedReader br = new BufferedReader(new FileReader(
                "C:\\Users\\Quynh\\Desktop\\PJV\\levaness\\GameEngine\\src\\main\\resources\\maps\\test02.txt"))) {
            int row = 0;
            String line;
            levels = new int[26][26];

            while ((line = br.readLine()) != null) {
                // Save remaining lines into levels array
                String[] parts = line.split(" ");
                for (int col = 0; col < parts.length; col++) {
                    int val = Integer.parseInt(parts[col]);
                    levels[row][col] = val;
                }
                row++;
            }
            if (log == true) {
                LOGGER.log(Level.INFO, "Loaded new level.");
            }
        } catch (IOException | NumberFormatException e) {
            if (log == true) {

                LOGGER.log(Level.SEVERE, "Error loading level data from default file", e);
            }
            e.printStackTrace();
        }
    }

    /**

     Loads game data from a JSON file and updates the player's information and information drawn out
     on the frame accordingly.
     The player's hearts, killed monsters count, keys, and coordinates are retrieved from the JSON object
     and applied to the player object.
     @param player The Player object to be updated with the loaded game data.
     @param JSON_FILE_PATH The file path of the JSON file containing the game data.
     */
    public void loadFromFile(Player player, String JSON_FILE_PATH) {
        json = loadJSONData(JSON_FILE_PATH);
        int hearts = json.getInt("player hearts");
        int killedMonsters = json.getInt("monsters killed");
        int keys = json.getInt("keys");
        int coordX = json.getInt("coordX");
        int coordY = json.getInt("coordY");

        player.setHearts(hearts);
        player.setKilledMonstersCount(killedMonsters);
        player.setKeyCount(keys);
        player.setX(coordX);
        player.setY(coordY);

        if (log == true) {

            LOGGER.log(Level.INFO, "Loaded player hearts: " + hearts + ", killed monsters: " + killedMonsters + ", keys: " + keys +
                    ", coordX: " + coordX + ", coordY: " + coordY);
        }
    }

    /**

     Loads and returns a JSONObject from the specified JSON file path.
     The JSON file is read using a FileReader and converted into a JSONObject using a JSONTokener.
     @param JSON_FILE_PATH The file path of the JSON file to be loaded.
     @return The loaded JSONObject from the specified JSON file path.
     */
    //package private for testing purposees
    JSONObject loadJSONData(String JSON_FILE_PATH) {
        try {
            FileReader reader = new FileReader(JSON_FILE_PATH);
            JSONTokener tokener = new JSONTokener(reader);
            return new JSONObject(tokener);
        } catch (IOException e) {
            if (log == true) {

                LOGGER.log(Level.SEVERE, "Error loading game data from file", e);
            }
            e.printStackTrace();
        }
        return null;
    }

    /**

     Saves the game data to a file.
     The player's information, including hearts, killed monsters count, keys, and coordinates,
     is stored in a JSON object and written to the specified file.
     @param player The Player object containing the game data to be saved.
     */
    public void saveToFile(Player player) {
        json = loadJSONData("src/main/resources/data/gameData.json");
        json.put("player hearts", player.getHearts());
        json.put("monsters killed", player.getKilledMonstersCount());
        json.put("keys", player.getKeyCount());
        json.put("coordX", player.getX());
        json.put("coordY", player.getY());

        try (FileWriter file = new FileWriter(fileName)) {
            file.write(json.toString());
            file.flush();
            if (log == true) {
                LOGGER.log(Level.INFO, "Saved data to file: " + json.toString());
            }
        } catch (IOException e) {
            if (log == true) {
                LOGGER.log(Level.SEVERE, "Error saving game data to file", e);
            }
            e.printStackTrace();
        }
    }

    /**

     Loads a game level from a file selected by the user.
     The level data is read from the file and stored in the levels array.
     This method is called to update the game's level data.
     */
    private void loadLevelFromFile() {
        if (panel.isGameEnded()){
            panel.setGameEnded(false);
        }
        JFileChooser fileChooser = new JFileChooser();
        int returnValue = fileChooser.showOpenDialog(this);
        levels = new int[26][26];

        if (returnValue == JFileChooser.APPROVE_OPTION) {
            try (BufferedReader br = new BufferedReader(new FileReader(fileChooser.getSelectedFile()))) {
                // File is selected, read the chosen file
                int row = 0;
                String line;


                while ((line = br.readLine()) != null) {
                        // Save remaining lines into levels array
                        String[] parts = line.split(" ");
                        for (int col = 0; col < parts.length; col++) {
                            int val = Integer.parseInt(parts[col]);
                            levels[row][col] = val;
                        }
                    row++;
                }
                setLevels(levels);
                if (log == true) {
                    LOGGER.log(Level.INFO, "Loaded level data from file: " + fileChooser.getSelectedFile().getName());
                }
            } catch (IOException | NumberFormatException e) {
                if (log == true) {
                    LOGGER.log(Level.SEVERE, "Error loading level data from file", e);
                }
                e.printStackTrace();
            }
        } else {
            try (BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\Quynh\\Desktop\\PJV\\levaness\\GameEngine\\src\\main\\resources\\maps\\test.txt"))) {
                int row = 0;
                String line;

                while ((line = br.readLine()) != null) {
                    // Save remaining lines into levels array
                    String[] parts = line.split(" ");
                    for (int col = 0; col < parts.length; col++) {
                        int val = Integer.parseInt(parts[col]);
                        levels[row][col] = val;
                    }
                    row++;
                }
                setLevels(levels);
//            LOGGER.log(Level.INFO, "Loaded level data from default file: test.txt");
            } catch (IOException | NumberFormatException e) {
                if (log == true) {
                    LOGGER.log(Level.SEVERE, "Error loading level data from default file", e);
                }
                e.printStackTrace();
            }
        }
        setLevels(levels);
    }

    /**

     Updates the game state by invoking the getLevels method.
     This method is responsible for updating the game levels or any related data.
     */
    public void update(){
        getLevels();
    }
    public boolean gameStarted(){
        return true;
    }
    public int[][] getLevels() {
        return levels;
    }
    public void setLevels(int[][] levels) {
        this.levels = levels;
    }

    public boolean isUpdateMap() {
        return updateMap;
    }

    public void setUpdateMap(boolean updateMap) {
        this.updateMap = updateMap;
    }
}