package cz.cvut.fel.pjv.Models;


import cz.cvut.fel.pjv.Entities.Player;
import cz.cvut.fel.pjv.MainPanel;

import java.util.logging.Level;
import java.util.logging.Logger;

public class PlayerService {
    private Player player;
    private Listener listener;
    private MapModel map;
    private LoadSave loadSave;
    private MainPanel panel;
    private static final Logger LOGGER = Logger.getLogger(PlayerService.class.getName());
    private boolean log;

    private int checkX, checkY;
    private int[][] matrix;

    /**

     Constructs a PlayerService object.
     @param player The Player object.
     @param listener The Listener object.
     @param loadSave The LoadSave object.
     @param map The MapModel object.
     @param panel The MainPanel object.
     */
    public PlayerService(Player player, Listener listener, LoadSave loadSave, MapModel map, MainPanel panel) {
        this.log = panel.isEnableLogging();
        this.player = player;
        this.listener = listener;
        this.loadSave = loadSave;
        this.map = map;
        this.panel = panel;
    }

    //zacinam na dvojce jako nule a na 25 koncim, jako by to byla 26
    /**
     * Handles collision between the player and the floor tiles based on the player's movement direction,
     * the tile value and the map borders.
     * Adjusts the player's position if a collision with a floor tile is not detected..
     */
    public void collisionWithFloor(){
        if (listener.isUp()) {

            checkX = player.getX()/24 - 2;
            checkY = player.getY()/24 - 1 -1;

            if (checkY <= 0) {
                player.setY(25 * 24 + 2*24 + 10);
            } else {
                check(checkX, checkY);
            }
        } else if (listener.isDown()) {
            checkX = player.getX()/24 - 2;
            checkY = player.getY()/24 + 1 - 1 -2;

            if (checkY >= 25) {
                player.setY(2*24);
            } else {
                check(checkX, checkY);
            }
            check(checkX, checkY);
        } else if (listener.isLeft()) {
            checkX = player.getX()/24 - 1 - 1;
            checkY = player.getY()/24 -2 ;

            if (checkX <= 0) {
                player.setX(25 * 24 + 2*24 + 10);
            } else {
                check(checkX, checkY);
            }
        } else if (listener.isRight()) {
            checkX = player.getX()/24 + 1 -3;
            checkY = player.getY()/24-2 ;

            if (checkX >= 25) {
                player.setX(2*24);

            } else {
                check(checkX, checkY);
            }
        }
    }

    /**
     * Performs a collision check based on the specified coordinates.
     * Handles collision with rocks, doors, and empty tiles.
     *
     * @param checkX The X-coordinate to check.
     * @param checkY The Y-coordinate to check.
     */
    private void check(int checkX, int checkY) {
        if (matrix[checkY][checkX] == 2) {
            // Code for collision with rocks

            player.getBounds().intersects(map.getBoundsOfRocks());

            resetCoords();

            if (log == true) {
                LOGGER.log(Level.INFO, "You've collided! The path doesn't continue in this direction!");
            }
        } else if (matrix[checkY][checkX] == 0) {
            // Code for empty tile
//            System.out.println("tile" + matrix[checkY][checkX]);

            if (player.getKeyCount() != player.getMaxKeys()) {
                player.getBounds().intersects(map.getBoundsOfDoors());

                resetCoords();
                if (log == true) {

                LOGGER.log(Level.INFO, "Collect more keys to open the door!");
                }
            } else if (player.getMaxKeys() <= player.getKeyCount() && player.getKilledMonstersCount() >= player.getMaxAmountOfMonsters()) {
                if (log == true) {

                    LOGGER.log(Level.INFO, "Congratulations! You've won!");
                }
                panel.setPlayerWon(true);
                panel.setGameEnded(true);
            } else {
                resetCoords();
                if (log == true) {

                    LOGGER.log(Level.INFO, "You still have to kill some monsters!");
                }
            }
        } else {

        }
    }

    /**
     * Resets the player's coordinates if a detection has been detected so it simulates the player collision.
     * This method is called when a collision occurs.
     */
    private void resetCoords() {
        if (listener.isUp()){
            player.setY(player.getY() + player.getSpeed());
        } else if (listener.isDown()){
            player.setY(player.getY() - player.getSpeed());
        } else if (listener.isLeft()){
            player.setX(player.getX() + player.getSpeed());
        } else if (listener.isRight()){
            player.setX(player.getX() - player.getSpeed());
        }
    }

    /**
     * Updates the player and checks for collisions with the floor.
     * It also updates the matrix if a new map is loaded.
     */
    public void update(){
        player.update();
        collisionWithFloor();
        if (loadSave.getLevels() == null){
//            System.out.println("levels is null");
        } else {
            matrix = loadSave.getLevels();
        }
    }
}
