package cz.cvut.fel.pjv.Models;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

public class Listener implements KeyListener {
    boolean left, up, down, right;
    Random rand = new Random();

    /**

     Handles the keyPressed event triggered by a KeyEvent.
     Sets the corresponding direction flags based on the key code received.
     @param e The KeyEvent object representing the key press event.
     */
    public void keyPressed(KeyEvent e) {
        int keyCode = e.getKeyCode();

        if (keyCode == KeyEvent.VK_W) {
            up = true;
        } else if (keyCode == KeyEvent.VK_A) {
            left = true;
        } else if (keyCode == KeyEvent.VK_S) {
            down = true;
        } else if (keyCode == KeyEvent.VK_D) {
            right = true;
        }

    }


    /**

     Handles the keyReleased event triggered by a KeyEvent.
     Resets the corresponding direction flags based on the key code received.
     @param e The KeyEvent object representing the key release event.
     */
    public void keyReleased(KeyEvent e) {
        int keyCode = e.getKeyCode();


        if (keyCode == KeyEvent.VK_W) {
            up = false;
        } else if (keyCode == KeyEvent.VK_A) {
            left = false;
        } else if (keyCode == KeyEvent.VK_S) {
            down = false;
        } else if (keyCode == KeyEvent.VK_D) {
            right = false;
        }

    }

    public void keyTyped(KeyEvent e) {
    }

    public boolean isLeft() {
        return left;
    }

    public boolean isUp() {
        return up;
    }

    public boolean isDown() {
        return down;
    }

    public boolean isRight() {
        return right;
    }

}
