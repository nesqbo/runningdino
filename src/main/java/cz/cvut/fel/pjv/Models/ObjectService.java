package cz.cvut.fel.pjv.Models;

import cz.cvut.fel.pjv.Entities.Food;
import cz.cvut.fel.pjv.Entities.Key;
import cz.cvut.fel.pjv.Entities.Player;

import java.awt.*;
import java.lang.reflect.Array;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ObjectService {
    private static final Logger LOGGER = Logger.getLogger(ObjectService.class.getName());

    private boolean log;
    private Key[] keys;
    private Food[] food;
    private Player player;
    private MapModel map;
    Random rand = new Random();
    /**

     Creates a new ObjectService with the specified player, keys, food, and map.
     @param player the player object
     @param keys the array of key objects
     @param food the array of food objects
     @param map the map model
     */
    public ObjectService(Player player, Key[] keys, Food[] food, MapModel map, boolean log){
        this.log = log;
        this.player = player;
        this.keys = keys;
        this.food = food;
        this.map = map;
    }

    /**

     Checks for collision between the player and keys, and updates the player's key count if a collision
     occurs.
     */
    public void collisionWithKey(){
        for (int i = 0; i < keys.length; i++) {
            if (keys[i] != null) {
                Key key = keys[i];
                if (player.getBounds().intersects(key.getBounds())) {
//            key.setCount(key.getCount() + 1);
                    player.setKeyCount(player.getKeyCount() + 1);
                    keys[i] = null;
//                    System.out.println("A key has been picked up!");
                }
            }
        }
    }

    /**

     Checks for collision between the player and food items, and updates the player's health by random if a
     collision occurs.
     */
    public void collisionWithFood(){
        for (int i =0; i<food.length; i++) {
            if (food[i]!=null) {
                if (player.getBounds().intersects(food[i].getBounds())) {
                    if (player.getHearts() < 100) {
                        player.setHearts(player.getHearts() + rand.nextInt(1, 50));
                        food[i] = null;
                    } else {
                        if (log == true) {
                            LOGGER.log(Level.FINEST,"You already have full health!");
                        }
                    }
                }
            }
        }
    }

    /**

     Updates the game state by checking for collision with food and keys.
     */
    public void update(){
        collisionWithFood();
        collisionWithKey();
    }

}
