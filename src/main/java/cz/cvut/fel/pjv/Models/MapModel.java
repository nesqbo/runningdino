package cz.cvut.fel.pjv.Models;

import cz.cvut.fel.pjv.Entities.Player;
import cz.cvut.fel.pjv.MainPanel;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static cz.cvut.fel.pjv.MainPanel.SIZE;

public class MapModel extends JPanel{
    private static final Logger LOGGER = Logger.getLogger(MapModel.class.getName());
    private boolean log;
    private int x = 0, y = 0;
    private BufferedImage[] images = new BufferedImage[3];
    private int[][] levels;
//    private int[][] defaultMap;
    private Player player;
    private LoadSave loadSave;
    private boolean updateMap;

    /**

     Constructs a MapModel object with the specified player and loadSave objects.
     This constructor initializes the MapModel by setting the player and loadSave references,
     and invokes the getTileImage method to load the tile images.
     @param player The Player object used in the game.
     @param loadSave The LoadSave object used to load and save game data.
     */
    public MapModel(Player player, LoadSave loadSave, boolean log){
        this.log = log;
        this.player = player;
        this.loadSave = loadSave;
        getTileImage();

    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    /**

     Draws the game map using the provided Graphics object.
     This method iterates over the levels array and draws the corresponding images for each tile.
     It also displays the row and column indices for each tile.
     @param g The Graphics object used for drawing.
     */
    public void draw(Graphics g) {
        if (!checkLevelMap()) {
//            readDefaultMap();
            System.out.println("level is null");
        }

        for (int col = 0; col < 26; col++) {
            for (int j = 0; j < 26; j++) {
                int x = j * SIZE + 60;
                int y = col * SIZE + 60;

                setPreferredSize(new Dimension(24, 24));
                Border border = BorderFactory.createLineBorder(Color.BLACK, 2);
                setBorder(border);


                g.drawImage(images[levels[col][j]], x, y, null);

                g.drawRect(x, y, 24, 24);

                String colString = String.format("%4d", col);
                int colStringWidth = g.getFontMetrics().stringWidth(colString);
                int colStringHeight = g.getFontMetrics().getHeight();
                g.drawString(colString, x + (24 - colStringWidth) / 2, y + (24 + colStringHeight) / 2);

                g.drawString(j + "  ", x, y + colStringHeight);
            }
        }
    }

    /**

     Checks if the level map is available for drawing.
     This method first checks if the level map is already loaded and available through the loadSave object.
     If it is, it returns true.
     If not, it checks if the levels array is null.
     If the levels array is null, it returns false.
     Otherwise, it returns true.
     @return true if the level map is and available, false otherwise.
     */
    boolean checkLevelMap() {
        if (loadSave.getLevels()!=null){
            return true;
        }
        if (levels == null){
            return false;
        } else {
            return true;
        }
    }

    /**

     Loads the tile images used in the game.
     This method reads the tile images from the corresponding resource paths,
     scales them to the specified size, and stores them in the images array.
     */
    public void getTileImage(){
        try {
            images[0] = ImageIO.read(getClass().getResourceAsStream("/floors/0.png"));//dvere
            images[1] = ImageIO.read(getClass().getResourceAsStream("/floors/1.png"));//grass
            images[2] = ImageIO.read(getClass().getResourceAsStream("/floors/2.png"));//kameny

            images[0] = scaleImage(images[0], SIZE, SIZE);
            images[1] = scaleImage(images[1], SIZE, SIZE);
            images[2] = scaleImage(images[2], SIZE, SIZE);
        } catch (IOException | NumberFormatException e) {
            if (log==true) {
                LOGGER.log(Level.SEVERE, "Error loading the floor tile.");
            }
            e.printStackTrace();
        }
    }

    /**

     Scales an image to the specified width and height (which is 24 ~ SIZE constant).
     @param originalImage The original image to be scaled.
     @param width The desired width of the scaled image.
     @param height The desired height of the scaled image.
     @return The scaled image.
     */
    BufferedImage scaleImage(BufferedImage originalImage, int width, int height) {
        BufferedImage scaledImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D graphics2D = scaledImage.createGraphics();
        graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics2D.drawImage(originalImage, 0, 0, width, height, null);
        graphics2D.dispose();
        return scaledImage;
    }

    //not used
    public Rectangle getBoundsOfRocks() {
        return new Rectangle(x, y, images[2].getWidth(), images[2].getHeight());
    }
    //not used
    public Rectangle getBoundsOfDoors(){
        return new Rectangle(x, y, images[0].getWidth(), images[0].getHeight());
    }

    /**

     Updates the levels array by fetching the current level data from the loadSave object.
     This method retrieves the levels array from the loadSave object and assigns it to the levels variable.
     It is called to update the levels array whenever there is a change in the level data.
     */
    public void update() {
        levels = loadSave.getLevels();
    }
}
