package cz.cvut.fel.pjv.Models;

import cz.cvut.fel.pjv.Entities.Monster;
import cz.cvut.fel.pjv.Entities.Player;
import cz.cvut.fel.pjv.MainPanel;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MonsterService {
    private final Logger LOGGER = Logger.getLogger(MonsterService.class.getName());
    private Random rand = new Random();
    private Monster[] monsters;
    private Player player;
    private MapModel map;
    private LoadSave loadSave;
    private int counter = 3, count = 3;
    private boolean gameEnded = false;
    private int[][] matrix;
    private boolean log;
    private MainPanel panel;
    /**

     Constructs a MonsterService object with the specified player, monsters, loadSave, and map objects.
     This constructor initializes the MonsterService by setting the player, monsters, loadSave, and map
     references.
     @param player The Player object used in the game.
     @param monsters An array of Monster objects representing the monsters in the game.
     @param loadSave The LoadSave object used to load and save game data.
     @param map The MapModel object representing the game map.
     */
    public MonsterService(Player player, Monster[] monsters, LoadSave loadSave, MapModel map, MainPanel panel){
        this.player = player;
        this.monsters = monsters;
        this.loadSave = loadSave;
        this.map = map;
        this.log = panel.isEnableLogging();
        this.panel = panel;
        }

    /**

     Updates the MonsterService.
     Checks if the levels are loaded from the loadSave object. If not, it logs an error message.
     Retrieves the levels matrix from the loadSave object and assigns it to the matrix variable.
     Initiates a fight between the player and the monsters.
     Moves the monsters.
     */
    public void update(){
        if (loadSave.getLevels() == null) {
            if (log == true) {
                LOGGER.log(Level.SEVERE, "The map has failed to load.");
            }
        } else {
            matrix = loadSave.getLevels();
        }
        fight();

        move(monsters);

    }

    /**

     Performs a fight between the player and the monsters.
     This method iterates through the monsters array and checks for collisions between the
     player and each monster.
     If a collision occurs, damage is dealt to both the player and the monster based on random values.
     The hearts of the monster and the player are updated accordingly.
     If the player's hearts reach zero, the game ends and a "You've died!" message is logged.
     If the monster's hearts reach zero, the monster is eliminated and the player's killedMonstersCount
     is incremented.
     If the player's killedMonstersCount reaches the maximum amount of monsters, it indicates a
     victory condition.
     */
    public void fight() {

        for (int i=0; i < monsters.length; i++) {

                if (monsters[i] != null) {
                        Monster monster = monsters[i];
                        if (player.getBounds().intersects(monster.getBounds())) {//TODO pridat int counter na pocet narazeni a pri tom menit pocet srdicek
                            monster.setDamage(rand.nextInt(0, 5));
                            player.setDamage(rand.nextInt(0, 2) + 1);

                            monster.setHearts(monster.getHearts() - player.getDamage());
                            player.setHearts((player.getHearts() - monster.getDamage()));

                            System.out.println(player.getHearts());
                            if (player.getHearts() <= 0) {

                                monsters[i] = null;
                                panel.setGameEnded(true);
                                if (log==true) {
                                    LOGGER.log(Level.INFO, "You've died!");
                                }
                            } else if (monster.getHearts() <= 0) {
                                player.setKilledMonstersCount(player.getKilledMonstersCount() + 1);
                                monsters[i] = null;
                                if (player.getKilledMonstersCount() >= player.getMaxAmountOfMonsters()) {
                                    if (log==true) {
                                        LOGGER.log(Level.INFO, "You've killed enough monsters, collect the rest of keys and unlock the door to win!");
                                    }
                                }
                        }
                    }
                }
            }
//        count--;// TODO jebathehehihihoho
    }


    public void move(Monster[] monsters) {
        for (Monster monster : monsters) {
            if (counter < 0) {
                if (monster != null) {
                    int direction = rand.nextInt(700)+90;

                    collisionCheck(monster);
                    if (direction <= 100) {
                        monster.moveRight();
                    } else if (direction <= 150){
                        monster.moveLeft();
                    } else if (direction <= 230){
                        monster.moveUp();
                    } else if (direction <=290){
                        monster.moveDown();
                    } else {
                    }
                }
                counter = 3;
            }
            counter--;
        }
    }

    /**

     Checks for collisions between the moving monster and its borders.
     If the moving monster encounters a tile which is on either of the four borders it can collide with,
     it moves in another direction so it can't move out of the playing board / out of reach of the player.
     @param movingMonster The monster that is being moved and checked for collisions.
     */
    private void collisionCheck(Monster movingMonster) {
        if (movingMonster.getY() / 26  -2 <= 0) {
            movingMonster.setY(25*24+3*24);
        } else if (movingMonster.getY() / 26 -2 >= 26) {
//            System.out.println(matrix[movingMonster.getY()/26][movingMonster.getX()/26]);

            movingMonster.setY(1*24+3*24);

        }else if (movingMonster.getX() / 26 -1 <= 0) {
//            System.out.println(matrix[movingMonster.getY()/26][movingMonster.getX()/26]);

            movingMonster.setX(25*24+3*24);

        }else if ((movingMonster.getX() / 26 -1) >= 26) {
//            System.out.println(matrix[movingMonster.getY()/26][movingMonster.getX()/26]);
            movingMonster.setX(1*24+3*24);

        }
    }

    /**

     Returns the status of the game.
     @return true if the game has ended, false otherwise.
     */
    //TODO CHECK
    public boolean isGameEnded() {
        return gameEnded;
    }
    public void setGameEnded(boolean gameEnded) {
        this.gameEnded = gameEnded;
    }

}
