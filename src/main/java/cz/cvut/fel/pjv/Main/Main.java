package cz.cvut.fel.pjv.Main;

import cz.cvut.fel.pjv.MainPanel;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        JFrame frame = new JFrame("DINOOOO TEST");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        MainPanel panel = new MainPanel();
        frame.add(panel);

        panel.startGame();
        frame.setSize(800, 800);
        frame.setVisible(true);
        frame.setResizable(true);
    }
}
