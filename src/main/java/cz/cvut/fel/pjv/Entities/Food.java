package cz.cvut.fel.pjv.Entities;

import cz.cvut.fel.pjv.Models.LoadSave;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Logger;

public class Food {
    private static final Logger LOGGER = Logger.getLogger(Food.class.getName());
    private int x, y;
    private BufferedImage image;
    private boolean active;
    private int count;
    Random rand = new Random();
    private boolean log;

    public Food(boolean log){
        this.log = log;
        active = false;
        try {
            image = ImageIO.read(getClass().getResourceAsStream("/objects/Whiskey.png"));
        } catch (IOException | NumberFormatException e) {
            if (log == true){
                LOGGER.severe("Error loading the food picture.");
            }
        }
    }

    /**

     Draws an image on a Graphics object at the specified coordinates.
     @param g The Graphics object on which to draw the image.
     @param x The x-coordinate of the image's horizontal position.
     @param y The y-coordinate of the image's vertical position.
     */
    public void draw(Graphics g, int x, int y) {
        setX(x); setY(y);
        g.drawImage(image, x, y, null);
    }

    /**

     Returns a Rectangle object representing the bounds of the image.
     The bounds include the position x, y and the size of the image.
     @return A Rectangle object representing the bounds of the image.
     */
    public Rectangle getBounds() {
        return new Rectangle(x, y, image.getWidth(), image.getHeight());
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
