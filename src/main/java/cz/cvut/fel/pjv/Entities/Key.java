package cz.cvut.fel.pjv.Entities;

import cz.cvut.fel.pjv.Models.LoadSave;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Key {
    private static final Logger LOGGER = Logger.getLogger(Key.class.getName());
    private int x = 168;
    private int y = 300;
    Random rand = new Random();
    private BufferedImage image;
    private boolean log;
    public Key(boolean log) {
        this.log = log;
        try {
            image = ImageIO.read(getClass().getResourceAsStream("/objects/key.png"));
        } catch (IOException | NumberFormatException e) {
            if (log==true) {
                LOGGER.log(Level.SEVERE, "Error loading the key picture.", e);
            }
            e.printStackTrace();
        }
    }

    /**

     Draws an image on a Graphics object coordinates specified in g.drawImage().
     @param g The Graphics object on which to draw the image.
     */
    public void draw(Graphics g) {
        g.drawImage(image, x, y, null);
    }

    /**

     Returns a Rectangle object representing the bounds of the image.
     The bounds include the position x, y and the size of the image.
     @return A Rectangle object representing the bounds of the image.
     */
    public Rectangle getBounds() {
        return new Rectangle(x, y, image.getWidth(), image.getHeight());
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
