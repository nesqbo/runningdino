package cz.cvut.fel.pjv.Entities;

import cz.cvut.fel.pjv.Models.Listener;
import cz.cvut.fel.pjv.Models.LoadSave;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Player {
    private BufferedImage playerImage;
    private int keyCount;
    private int maxKeys = 3;
    private static final Logger LOGGER = Logger.getLogger(Player.class.getName());
    private int x = 60, y = 60;
    private int xOffset = 0;
    private int yOffset = 0;
    private int hearts = 100;
    private int damage = 0;
    private int speed = 3;
    private Listener listener;
    private int killedMonstersCount = 0;
    private int maxAmountOfMonsters = 3;
    public Player(Listener listener) {
        createPlayerImage();
        this.listener = listener;
    }
    /**

     Retrieves and returns the BufferedImage object representing the monster image.
     The image is loaded from the resource file "/player/crying2.png".
     @return The BufferedImage object representing the monster image.
     @throws IOException if an error occurs while reading the image file.
     @throws NumberFormatException if the image file contains invalid format or data.
     */
    private BufferedImage createPlayerImage() {
        try {
            playerImage = ImageIO.read(getClass().getResourceAsStream("/player/crying2.png"));

        } catch (IOException | NumberFormatException e) {
            LOGGER.log(Level.SEVERE, "Error loading the monster picture.");
            e.printStackTrace();
        }
        return playerImage;
    }
    /**

     Updates the position of the object based on the input received from the listener.
     The object's position is adjusted according to the speed and the direction specified by the listener.
     */
    public void update() {
        if (listener.isUp()) {
            setY(getY() - speed);
        } else if (listener.isDown()) {
            setY(getY() + speed);
        } else if (listener.isLeft()) {
            setX(getX() - speed);
        } else if (listener.isRight()) {
            setX(getX() + speed);
        }


    }

    /**

     Returns a Rectangle object representing the bounds of the image.
     The bounds include the position x, y and the size of the image.
     @return A Rectangle object representing the bounds of the image.
     */
    public Rectangle getBounds() {
        return new Rectangle(x, y, playerImage.getWidth(), playerImage.getHeight());
    }
    public int getHearts(){
        return hearts;
    }
    public void setHearts(int hearts) {
        this.hearts = hearts;
    }
    public int getDamage() {
        return damage;
    }
    public void setDamage(int damage) {
        this.damage = damage; // = rand.nextInt(0, 5) + 1
    }
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
    public void draw(Graphics g){
        g.drawImage(playerImage, x, y,24,24, null);
    }
    public void setX(int x) {
        this.x = x;
    }
    public void setY(int y) {
        this.y = y;
    }
    public int getKeyCount() {
        return keyCount;
    }
    public void setKeyCount(int keyCount) {
        this.keyCount = keyCount;
    }
    public int getKilledMonstersCount() {
        return killedMonstersCount;
    }
    public void setKilledMonstersCount(int killedMonsterCount) {
        this.killedMonstersCount = killedMonsterCount;
    }
    public int getMaxAmountOfMonsters() {
        return maxAmountOfMonsters;
    }
    public void setMaxAmountOfMonsters(int maxAmountOfMonsters) {
        this.maxAmountOfMonsters = maxAmountOfMonsters;
    }
    public int getSpeed() {
        return speed;
    }
    public void setSpeed(int speed) {
        this.speed = speed;
    }
    public int getMaxKeys() {
        return maxKeys;
    }
    public void setMaxKeys(int maxKeys) {
        this.maxKeys = maxKeys;
    }
}
