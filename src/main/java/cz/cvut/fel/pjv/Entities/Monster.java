package cz.cvut.fel.pjv.Entities;

import cz.cvut.fel.pjv.Models.LoadSave;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Monster {
    private static final Logger LOGGER = Logger.getLogger(Monster.class.getName());
    private int x = 350;
    private int y = 356;
    private BufferedImage image;
    private boolean active;
    private Random random = new Random();
    private int speed = 2;
    private int hearts = 30;
    private int damage;//the damage that it gives to the player
private boolean log;

    public Monster(boolean log) {
        getImage();
        this.x = x;
        this.y = y;
        this.log = log;
    }
    /**

     Moves the object to the left by updating its x-coordinate based on its speed.
     This method is represents leftward movement of the object.
     */
    public void moveLeft() {
        setX(getX() - getSpeed());
    }
    /**

     Moves the object to the right by updating its x-coordinate based on its speed.
     This method is represents roghtward movement of the object.
     */
    public void moveRight() {
        setX(getX() + getSpeed());
    }
    /**

     Moves the object up by updating its y-coordinate based on its speed.
     This method is represents upward movement of the object.
     */
    public void moveUp() {
        setY(getY() - getSpeed());
    }
    /**

     Moves the object down by updating its y-coordinate based on its speed.
     This method is represents downward movement of the object.
     */
    public void moveDown() {
        setY(getY() + getSpeed());
    }


    /**

     Retrieves and returns the BufferedImage object representing the monster image.
     The image is loaded from the resource file "/monsters/monster.png".
     @return The BufferedImage object representing the monster image.
     @throws IOException if an error occurs while reading the image file.
     @throws NumberFormatException if the image file contains invalid format or data.
     */
    public BufferedImage getImage() {
        try {
            image = ImageIO.read(getClass().getResourceAsStream("/monsters/monster.png"));
        } catch (IOException | NumberFormatException e) {
            if (log==true) {
                LOGGER.log(Level.SEVERE, "Error loading the monster picture.");
            }
            e.printStackTrace();
        }
        return image;
    }

    /**

     Draws an image on a Graphics object at the specified in g.drawImage().
     Draws the entity state on the y coordinate + 3.
     @param g The Graphics object on which to draw the image.
     */
    public void draw(Graphics g) {
        g.drawImage(image, x, y, null);
        g.setFont(new Font("Ariel", 16, 16));
        g.drawString( "" + getHearts(), x, y+3);

        //monster spawner
//        g.setColor(Color.RED);
//        g.drawRect(324, 348, 24, 24);//mozna to pak zmenim aby to bylo primo v mape idk
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setX(int x) {
        this.x = x;
    }
    public void setY(int y) {
        this.y = y;
    }
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }

    /**

     Returns a Rectangle object representing the bounds of the image.
     The bounds include the position x, y and the size of the image.
     @return A Rectangle object representing the bounds of the image.
     */
    public Rectangle getBounds() {
        return new Rectangle(x, y, image.getWidth(), image.getHeight());
    }

    public int getHearts() {
        return hearts;
    }

    public void setHearts(int hearts) {
        this.hearts = hearts;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public void update() {}
}
