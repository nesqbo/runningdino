package cz.cvut.fel.pjv;

import cz.cvut.fel.pjv.Entities.Key;
import cz.cvut.fel.pjv.Entities.Player;
import cz.cvut.fel.pjv.Models.LoadSave;

import java.awt.*;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

//it is not being used idk why im not deleting it
public class InventoryPanel extends JPanel {
    private static final Logger LOGGER = Logger.getLogger(InventoryPanel.class.getName());


    private JLabel[] itemLabels;
    Player player;
    public InventoryPanel(Player player) {

        this.player = player;
    }
    public void draw(Graphics g){
        setPreferredSize(new Dimension(400, 50));

        // Set up the border for the inventory panel
        Border border = BorderFactory.createLineBorder(Color.BLACK, 2);
        setBorder(border);

        JPanel itemPanel = new JPanel(new GridLayout(1, 3));

        for (int i = 0; i < player.getKeyCount(); i++) {
            g.setColor(Color.RED);
            g.fillRect(50+i*100, 100, 100, 100);
        }

        // Add the item panel to the inventory panel
        add(itemPanel, BorderLayout.CENTER);

    }
    public void setItemImage(int index, ImageIcon imageIcon) {
        itemLabels[index].setIcon(imageIcon);
    }

}
