package cz.cvut.fel.pjv.Models;

import cz.cvut.fel.pjv.Entities.Player;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import static org.mockito.Mockito.*;

public class LoadSaveTest {
    private Player player = mock(Player.class);
    private JSONObject json = Mockito.mock(JSONObject.class);
    private LoadSave loadSave;
    private Listener listener;

    @BeforeEach
    public void setUp(){
        player = mock(Player.class);
        loadSave = mock(LoadSave.class);
        listener = mock(Listener.class);
    }
    @Test
    public void testLoadFromFile() {
        Player player = new Player(listener);
        String jsonFilePath = "src/test/java/cz/cvut/fel/pjv/testDB.json";

        loadSave.loadFromFile(player, jsonFilePath);

        // Assert that player attributes have been properly loaded
        Assertions.assertEquals(100, player.getHearts());
        Assertions.assertEquals(0, player.getKilledMonstersCount());
        Assertions.assertEquals(0, player.getKeyCount());
        Assertions.assertEquals(60, player.getX());
        Assertions.assertEquals(60, player.getY());
    }


}
