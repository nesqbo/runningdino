package cz.cvut.fel.pjv.Models;

import cz.cvut.fel.pjv.Entities.Player;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MapModelTest {
    private Player player;
    private LoadSave loadSave;
    private MapModel mapModel;

    @BeforeEach
    public void setUp(){
        player = mock(Player.class);
        loadSave = mock(LoadSave.class);
        mapModel = new MapModel(player, loadSave, false);
    }

    @Test
    void testCheckLevelMap_WhenLevelMapIsNotLoaded() {
        assertFalse(mapModel.checkLevelMap());
    }

    @Test
    void testGetTileImage() {
        assertDoesNotThrow(() -> mapModel.getTileImage());
    }

    @Test
    void testScaleImage() {
        BufferedImage originalImage = new BufferedImage(100, 100, BufferedImage.TYPE_INT_ARGB);
        BufferedImage scaledImage = mapModel.scaleImage(originalImage, 50, 50);
        assertNotNull(scaledImage);
        assertEquals(50, scaledImage.getWidth());
        assertEquals(50, scaledImage.getHeight());
    }

//    @Test
//    void testUpdate() {
//        int[][] a = new int[26][26];
//        mapModel.update();
//        loadSave.readDefaultMap();
//        a = loadSave.getLevels();
//        assertNotNull(a);
//    }

//    @Test
//    void testCheckLevelMap_WhenLevelMapIsLoaded() {
//        loadSave.setLevels(new int[10][10]);
//        assertTrue(mapModel.checkLevelMap());
//    }
}
