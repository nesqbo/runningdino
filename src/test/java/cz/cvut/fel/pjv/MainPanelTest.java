package cz.cvut.fel.pjv;

import cz.cvut.fel.pjv.Entities.Food;
import cz.cvut.fel.pjv.Entities.Key;
import cz.cvut.fel.pjv.Entities.Monster;
import cz.cvut.fel.pjv.Entities.Player;
import cz.cvut.fel.pjv.Models.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import javax.swing.*;
import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class MainPanelTest {

    private MainPanel panel;
    private MapModel map;
    private Food[] food;
    private Monster[] monster;
    private Key[] key;
    private Listener listener;
    private Player player;
    private LoadSave loadSave;
    private PlayerService playerService;
    private MonsterService monsterService;
    private ObjectService objectService;
    @BeforeEach
    public void setUp(){
        panel = mock(MainPanel.class);
        loadSave = mock(LoadSave.class);
        map = mock(MapModel.class);
        playerService = mock(PlayerService.class);
        monsterService = mock(MonsterService.class);
        objectService = mock(ObjectService.class);
        monster = new Monster[]{mock(Monster.class)};
    }

    @Test
    public void testUpdateMonsterCount() {
        LoadSave mockLoadSave = Mockito.mock(LoadSave.class);
        Player mockPlayer = Mockito.mock(Player.class);
        Monster[] mockMonsters = new Monster[5];

        MainPanel mainPanel = Mockito.mock(MainPanel.class);

        // Set up the player's maximum amount of monsters and killed monsters count
        when(mockPlayer.getMaxAmountOfMonsters()).thenReturn(5);
        when(mockPlayer.getKilledMonstersCount()).thenReturn(3);

        // Call the method
        mainPanel.updateMonsterCount();

        // Assert that the monster array has been properly updated
        Assertions.assertEquals(5, mockMonsters.length);

        // Verify that the setMonsters() method is called
        verify(mainPanel, times(1)).updateMonsterCount();
    }

    @Test
    public void testUpdateKeyCountTest() {
        LoadSave mockLoadSave = Mockito.mock(LoadSave.class);
        Player mockPlayer = Mockito.mock(Player.class);
        Key[] mockKeys = new Key[3];

        MainPanel mainPanel = Mockito.mock(MainPanel.class);

        // Set up the player's key count and maximum keys
        when(mockPlayer.getKeyCount()).thenReturn(0);
        when(mockPlayer.getMaxKeys()).thenReturn(3);

        // Call the method
        mainPanel.updateKeyCount();

        // Assert that the key array has been properly updated
        Assertions.assertEquals(3, mockKeys.length);
        Assertions.assertEquals(0, mockPlayer.getKeyCount());

        verify(mainPanel, times(1)).updateKeyCount();

    }


    @Test
    public void testSetKeysTwo() {
        MainPanel keySetter = Mockito.mock(MainPanel.class);

        // Call the method
        keySetter.setKeys();

        verify(keySetter, times(1)).setKeys();
    }

    @Test
    public void testUpdateObjects() {
        LoadSave mockLoadSave = Mockito.mock(LoadSave.class);
        Player mockPlayer = Mockito.mock(Player.class);
        MainPanel mainPanel = Mockito.mock(MainPanel.class);

        // Mock the behavior of the loadSave object
        when(mockLoadSave.isUpdateMap()).thenReturn(true);

        // Call the method
        mainPanel.updateObjects();

        // Verify that the updateKeyCount() and updateMonsterCount() methods are called
        verify(mainPanel, times(1)).updateObjects();

    }


    @Test
    void testUpdateObjectsTest() {
        MainPanel mainPanel = new MainPanel();
        mainPanel.updateObjects();

        assertFalse(loadSave.isUpdateMap());

    }

    @Test
    void testSetMonsters() {
        MainPanel mainPanel = new MainPanel();
        mainPanel.setMonsters();

        assertNotNull(mainPanel.getMonster()[0]);

    }



//    @Test
//    void testRun() {
//        MainPanel mainPanel = new MainPanel();
//        mainPanel.setGameEnded(false);
//
//        // Create a mock Graphics object
//        Graphics g = Mockito.mock(Graphics.class);
//
//        // Call the run method in a separate thread
//        Thread thread = new Thread(mainPanel::run);
//        thread.start();
//
//        // Sleep for a short duration to allow the game loop to run
//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//        // Interrupt the thread to stop the game loop
//        thread.interrupt();
//
//        assertTrue(mainPanel.isGameEnded());
//        verify(g, atLeastOnce()).dispose();
//
//    }

//    @Test
//    void testStartGame() {
//        MainPanel mainPanel = new MainPanel();
//
//        // Create a mock Graphics object
//        Graphics g = Mockito.mock(Graphics.class);
//
//        // Create a mock JFrame object
//        JFrame frame = Mockito.mock(JFrame.class);
//        when(frame.getGraphics()).thenReturn(g);
//
//        mainPanel.setGameEnded(true);
//
//        mainPanel.startGame();
//
//        assertNotNull(mainPanel.getGameThread());
//        assertTrue(mainPanel.getGameThread().isAlive());
//        assertFalse(mainPanel.isGameEnded());
//
//    }
}
